#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <ctime>
#include <stdint.h>
#include <libspe2.h>
#include <pthread.h>
#include <ppu_intrinsics.h>

#include "MemoryMapped.h"
#include "ppm.h"

#define imageRows 480
#define imageCols 640
#define chunkRows 240

const int TICKS_PER_SEC = 79800000;
//////////////////////////////////////////////////////////////////////
//                               TIMING                             //
//////////////////////////////////////////////////////////////////////
void startTimer(unsigned long &startTime)
{
	startTime = __mftb();
}

double endTimer(unsigned long startTime)
{
	double totalTime;
	unsigned long endTime = __mftb();
	totalTime = (endTime - startTime)/TICKS_PER_SEC;
	return totalTime;
}
//////////////////////////////////////////////////////////////////////
//                               SOBEL PPU                          //
//////////////////////////////////////////////////////////////////////
//
//uint16_t output[480][640];
//ppm *imageWriter = new ppm(640, 480);

/*void sobel(uint8_t(&dataGrid)[480][640])
{
	int8_t GX[3][3];
	int8_t GY[3][3];
	//uint16_t output[480][640];
	int16_t gxVal;
	int16_t gyVal;
	uint16_t sobelValue;

	/* 3x3 GX Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
/*	GX[0][0] = -1; GX[0][1] = 0; GX[0][2] = 1;
	GX[1][0] = -2; GX[1][1] = 0; GX[1][2] = 2;
	GX[2][0] = -1; GX[2][1] = 0; GX[2][2] = 1;

	/* 3x3 GY Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
/*	GY[0][0] = 1; GY[0][1] = 2; GY[0][2] = 1;
	GY[1][0] = 0; GY[1][1] = 0; GY[1][2] = 0;
	GY[2][0] = -1; GY[2][1] = -2; GY[2][2] = -1;


	ppm *imageWriter = new ppm(640, 480);

	for (int rows = 0; rows < 480; ++rows)
	{
		for (int cols = 0; cols < 640; ++cols)
		{
			//middle pixel
			gxVal = dataGrid[rows][cols] * GX[1][1];
			gyVal = dataGrid[rows][cols] * GY[1][1];
			//above middle pixel
			gxVal += dataGrid[rows - 1][cols] * GX[0][1];
			gyVal += dataGrid[rows - 1][cols] * GY[0][1];
			//top left pixel
			gxVal += dataGrid[rows - 1][cols - 1] * GX[0][0];
			gyVal += dataGrid[rows - 1][cols - 1] * GY[0][0];
			//left middle pixel
			gxVal += dataGrid[rows][cols - 1] * GX[1][0];
			gyVal += dataGrid[rows][cols - 1] * GY[1][0];
			//bottom left pixel
			gxVal += dataGrid[rows + 1][cols - 1] * GX[2][0];
			gyVal += dataGrid[rows + 1][cols - 1] * GY[2][0];
			//below middle pixel
			gxVal += dataGrid[rows + 1][cols] * GX[2][1];
			gyVal += dataGrid[rows + 1][cols] * GY[2][1];
			//bottom right pixel
			gxVal += dataGrid[rows + 1][cols + 1] * GX[2][2];
			gyVal += dataGrid[rows + 1][cols + 1] * GY[2][2];
			//middle right pixel
			gxVal += dataGrid[rows][cols + 1] * GX[1][2];
			gyVal += dataGrid[rows][cols + 1] * GY[1][2];
			//top right pixel
			gxVal += dataGrid[rows + 1][cols - 1] * GX[0][2];
			gyVal += dataGrid[rows + 1][cols - 1] * GY[0][2];

			gxVal = gxVal / 9;
			gyVal = gyVal / 9;
			sobelValue = sqrt(pow(gxVal, 2) + pow(gyVal, 2));
			output[rows][cols] = sobelValue;
		}
	}

	int k = 0;

	for (int i = 0; i < 480; ++i)
	{
		for (int j = 0; j < 640; ++j)
		{
			imageWriter->r[k] = output[i][j];
			imageWriter->g[k] = output[i][j];
			imageWriter->b[k] = output[i][j];
			++k;
		}
	}
	imageWriter->write("output.ppm");
}*/



//////////////////////////////////////////////////////////////////////
//                               MEDIAN PPU                         //
//////////////////////////////////////////////////////////////////////

/*void median()
{
	uint16_t median[49];
	uint16_t sortedMedian[49];
	uint16_t medianOutput[480][640];
	signed char rowToAdd = -3;
	int i = 0;
	for (int rows = 0; rows < 480; ++rows)
	{
		for (int cols = 0; cols < 640; ++cols)
		{
			i = 0;
			while (rowToAdd != 3)
			{
				median[i] = output[rows + rowToAdd][cols - 3];
				++i;
				median[i] = output[rows + rowToAdd][cols - 2];
				++i;
				median[i] = output[rows + rowToAdd][cols - 1];
				++i;
				median[i] = output[rows + rowToAdd][cols];
				++i;
				median[i] = output[rows + rowToAdd][cols + 1];
				++i;
				median[i] = output[rows + rowToAdd][cols + 2];
				++i;
				median[i] = output[rows + rowToAdd][cols + 3];
				++i;
				++rowToAdd;
			}
			rowToAdd = -3;

			for (int i = 0; i < 49; ++i)
			{
				if (median[i] > 49)
				{
					median[i] = 0;
				}
				sortedMedian[i] = median[i];
			}

			for (int i = 49; i > 0; --i) {
				for (int j = 0; j < i; ++j) {
					if (sortedMedian[j] > sortedMedian[j + 1])
					{
						double dTemp = sortedMedian[j];
						sortedMedian[j] = sortedMedian[j + 1];
						sortedMedian[j + 1] = dTemp;
					}
				}
			}
			int dMedian;
			dMedian = sortedMedian[48 / 2];
			medianOutput[rows][cols] = dMedian;
		}
	}

	int k = 0;

	for (int i = 0; i < 480; ++i)
	{
		for (int j = 0; j < 640; ++j)
		{
			imageWriter->r[k] = medianOutput[i][j];
			imageWriter->g[k] = medianOutput[i][j];
			imageWriter->b[k] = medianOutput[i][j];
			++k;
		}
	}
	imageWriter->write("medianOutput.ppm");
}*/

typedef struct ppu_thread_data{
	spe_context_ptr_t speID;
	pthread_t pThread;
	void *argp;
} ppu_pthread_data_t;

void *ppu_pthread_function(void *arg)
{
	ppu_pthread_data_t *data = (ppu_pthread_data_t *)arg;
	int retVal;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	if((retVal = spe_context_run(data->speID, &entry, 0, data->argp, NULL, NULL)) < 0)
	{
		perror("spe_context_run");
		exit(1);	
	}	
}

//////////////////////////////////////////////////////////////////////
//                               LOAD FILE                          //
//////////////////////////////////////////////////////////////////////
void loadFile(uint8_t(&array)[10][480][640], std::string *files)
{
	for(int i = 0; i < 10; ++i)
	{
	MemoryMapped file(files[i].c_str(), MemoryMapped::WholeFile, MemoryMapped::SequentialScan);
	if (!file.isValid())
	{
		printf("File not found\n");
	}

	const unsigned char* pointer = file.getData();

	std::string current;

	int row = 0;
	int column = 0;

	while (*pointer != '\0')
	{
		if (*pointer == ',')
		{
			current = "";
		}
 		else
		{
			current += *pointer;

			const unsigned char* pointer2 = pointer;
			++pointer2;
			if (*pointer2 == ',' || *pointer2 == 'r')
			{
				array[i][row][column] = atoi(current.c_str());
				column++;
			}
		}
		
		if (*pointer == '\r')
		{
			row++;
			column = 0;
			if (*pointer == '\n')
			{
				++pointer;
			}
		}
		pointer++;
	}
	}
}

//////////////////////////////////////////////////////////////////////
//                               INIT SPUS                          //
//////////////////////////////////////////////////////////////////////
void SPUInit(int &numSpus, spe_program_handle_t spu, ppu_pthread_data_t *data)
{
	int retVal;
	
	numSpus = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, 0);
	
	for(int i = 0; i < numSpus; ++i)
	{
		data[i].speID = spe_context_create(0, NULL);
		if(!data[i].speID)
		{
			perror("spe_context_create");
			exit(1);
		}

		retVal = spe_program_load(data[i].speID, &spu);
		if(retVal)
		{
			perror("spe_program_load");
			exit(1);
		}
	}
}

//////////////////////////////////////////////////////////////////////
//                               SOBEL SPU                          //
//////////////////////////////////////////////////////////////////////
void processSobel(uint8_t(&array)[10][480][640], int numSpus, ppu_pthread_data_t *data)
{
	unsigned char *chunkPointer;
	unsigned char *otherChunkPointer;
	unsigned char sobelOutput[imageRows*imageCols];
	int retVal;
	int dataToBeSent;
	int chunkSize = 1024*15;
	std::string ppmFileNames[10];

	ppmFileNames[0] = "sobelOutput1.ppm";
	ppmFileNames[1] = "sobelOutput2.ppm";
	ppmFileNames[2] = "sobelOutput3.ppm";
	ppmFileNames[3] = "sobelOutput4.ppm";
	ppmFileNames[4] = "sobelOutput5.ppm";
	ppmFileNames[5] = "sobelOutput6.ppm";
	ppmFileNames[6] = "sobelOutput7.ppm";
	ppmFileNames[7] = "sobelOutput8.ppm";
	ppmFileNames[8] = "sobelOutput9.ppm";
	ppmFileNames[9] = "sobelOutput10.ppm";

	printf("Array size: %d\n", (sizeof(array)/1024));
	unsigned char chunk[chunkRows*imageCols]__attribute__((aligned(128)));
	printf("Chunk Size: %d\n", (sizeof(chunk)/1024));
	unsigned char otherChunk[chunkRows*imageCols]__attribute__((aligned(128)));

	double timeTaken;
	for(int files = 0; files < 10; ++files)
	{
		for(uint16_t i = 0; i < chunkRows; ++i)
		{
			for(uint16_t j = 0; j < imageCols; ++j)
			{
				chunk[imageCols * i + j] = array[files][i][j];
				otherChunk[imageCols * i + j] = array[files][i + chunkRows][j] ;
			}
		}
	dataToBeSent = 1024 * 150;
	chunkPointer = chunk;
	otherChunkPointer = otherChunk;
	int counter = 0;
	int otherCounter = 10;
	while(dataToBeSent != 0)
	{
		data[0].argp = chunkPointer;
		if((retVal = pthread_create(&data[0].pThread, NULL, &ppu_pthread_function, &data[0].speID)) !=0)
		{
			perror("pthread_join");
			exit(1);
		}
		data[1].argp = otherChunkPointer;
		if((retVal = pthread_create(&data[1].pThread, NULL, &ppu_pthread_function, &data[1].speID)) !=0)
		{
			perror("pthread_join");
			exit(1);
		}
		if((retVal = pthread_join(data[0].pThread, NULL)) != 0)
		{
			perror("pthread_join");
			exit(1);
		}	
		if((retVal = pthread_join(data[1].pThread, NULL)) != 0)
		{
			perror("pthread_join");
			exit(1);
		}	
		std::memmove(sobelOutput + (counter * chunkSize), chunkPointer, (size_t)chunkSize);
		std::memmove(sobelOutput + (otherCounter * chunkSize), otherChunkPointer, (size_t)chunkSize);
		otherChunkPointer += chunkSize;
		chunkPointer += chunkSize;
		dataToBeSent -= chunkSize;
		++counter;
		++otherCounter;
	}
	ppm* imageWriter = new ppm(imageCols, imageRows);
	int k = 0;
	
	for (int i = 0; i < imageRows; ++i)
	{
		for (int j = 0; j < imageCols; ++j)
		{
			imageWriter->r[k] = sobelOutput[imageCols * i + j];
			imageWriter->g[k] = sobelOutput[imageCols * i + j];
			imageWriter->b[k] = sobelOutput[imageCols * i + j];
			++k;
		}
	}
	imageWriter->write(ppmFileNames[files]);
	}
}

//////////////////////////////////////////////////////////////////////
//                               MAIN                               //
//////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
	float totalTime;
	for(int i = 0; i < 10; ++i)
	{
	clock_t startTime = clock();
	extern spe_program_handle_t spu;
	ppu_pthread_data_t data[16];
	double time;
	//unsigned long startTime;
	int spus, retval;
	uint8_t array[10][imageRows][imageCols];
	std::string fileNames[10];

	fileNames[0] = "Frame1.csv";
	fileNames[1] = "Frame2.csv";
	fileNames[2] = "Frame3.csv";
	fileNames[3] = "Frame4.csv";
	fileNames[4] = "Frame5.csv";
	fileNames[5] = "Frame6.csv";
	fileNames[6] = "Frame7.csv";
	fileNames[7] = "Frame8.csv";
	fileNames[8] = "Frame9.csv";
	fileNames[9] = "Frame10.csv";
	
	SPUInit(spus, spu, data);
	loadFile(array, fileNames);
	processSobel(array, spus, data);

	for(int i = 0; i < spus; ++i)
	{
		retval = spe_context_destroy(data[i].speID);
		if(retval)
		{
			perror("spe_context_destroy");
			exit(1);
		}
	}
	clock_t endTime = clock();
	totalTime += (float)(endTime - startTime);
	}
	printf("time taken: %f\n", totalTime/CLOCKS_PER_SEC/10);
	return 0;
}
