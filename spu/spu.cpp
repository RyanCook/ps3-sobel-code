#include <stdio.h>
#include <stdint.h>
#include <spu_mfcio.h>
#include <math.h>

#define TAG 31

void sobel(unsigned char *dataGrid, unsigned char(&output)[24 * 640])
{
	int GX[3][3];
	int GY[3][3];
	int gxVal;
	int gyVal;
	int sobelValue;
	int centreGx;
	int centreGy;

	/* 3x3 GX Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
	GX[0][0] = -1; GX[0][1] = 0; GX[0][2] = 1;
	GX[1][0] = -2; GX[1][1] = 0; GX[1][2] = 2;
	GX[2][0] = -1; GX[2][1] = 0; GX[2][2] = 1;

	/* 3x3 GY Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
	GY[0][0] = 1; GY[0][1] = 2; GY[0][2] = 1;
	GY[1][0] = 0; GY[1][1] = 0; GY[1][2] = 0;
	GY[2][0] = -1; GY[2][1] = -2; GY[2][2] = -1;

	for (int rows = 0; rows < 24; ++rows)
	{
		for (int cols = 0; cols < 640; ++cols)
		{
			//middle pixe
			gxVal = dataGrid[640 * rows + cols] * GX[1][1];
			gyVal = dataGrid[640 * rows + cols] * GY[1][1];
			//above middle pixel
			if(rows!=0)
			{
				gxVal += dataGrid[640 * (rows - 1) + cols] * GX[0][1];
				gyVal += dataGrid[640 * (rows - 1) + cols] * GY[0][1];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[0][1];
				gyVal += dataGrid[640 * rows + cols] * GY[0][1];

			}
			//top left pixel
			if(rows != 0 && cols != 0)
			{
				gxVal += dataGrid[640 * (rows - 1) + (cols - 1)] * GX[0][0];
				gyVal += dataGrid[640 * (rows - 1) + (cols - 1)] * GY[0][0];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[0][0];
			        gyVal += dataGrid[640 * rows + cols] * GY[0][0];
			}
			//left middle pixel
			if(cols != 0)
			{
				gxVal += dataGrid[640 * rows + (cols - 1)] * GX[1][0];
				gyVal += dataGrid[640 * rows + (cols - 1)] * GY[1][0];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[1][0];
				gyVal += dataGrid[640 * rows + cols] * GY[1][0];
			}
			//bottom left pixel
			if(rows != 23 && cols != 0)
			{
				gxVal += dataGrid[640 * (rows + 1) + (cols - 1)] * GX[2][0];
				gyVal += dataGrid[640 * (rows + 1) + (cols - 1)] * GY[2][0];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[2][0];
				gyVal += dataGrid[640 * rows + cols] * GY[2][0];
			}
			//below middle pixel
			if(rows != 23)
			{
				gxVal += dataGrid[640 * (rows + 1) + cols] * GX[2][1];
				gyVal += dataGrid[640 * (rows + 1) + cols] * GY[2][1];
	 		}			
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[2][1];
				gyVal += dataGrid[640 * rows + cols] * GY[2][1];
			}
			//bottom right pixel
			if(rows != 23 && cols != 639)
			{
				gxVal += dataGrid[640 * (rows + 1) + (cols + 1)] * GX[2][2];
				gyVal += dataGrid[640 * (rows + 1) + (cols + 1)] * GY[2][2];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[2][2];
				gyVal += dataGrid[640 * rows + cols] * GY[2][2];
			}
			//middle right pixel
			if(cols != 639)
			{
				gxVal += dataGrid[640 * rows + (cols + 1)] * GX[1][2];
				gyVal += dataGrid[640 * rows + (cols + 1)] * GY[1][2];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[1][2];
				gyVal += dataGrid[640 * rows + cols] * GY[1][2];
			}
			//top right pixel
			if(rows != 0 && cols != 639)
			{
				gxVal += dataGrid[640 * (rows - 1) + (cols + 1)] * GX[0][2];
				gyVal += dataGrid[640 * (rows - 1) + (cols + 1)] * GY[0][2];
			}
			else
			{
				gxVal += dataGrid[640 * rows + cols] * GX[0][2];
				gyVal += dataGrid[640 * rows + cols] * GY[0][2];
			}

			gxVal = gxVal / 9;
			gyVal = gyVal / 9;
			sobelValue = sqrt(pow(gxVal, 2) + pow(gyVal, 2));
			output[640 * rows + cols] = sobelValue;
			//printf("Sobel: %d : ", sobelValue);
		}
	}
}

int main(unsigned long long spe_id, unsigned long long argp, unsigned long long envp)
{
	unsigned char buff[24 * 640]__attribute__ ((aligned(128)));
	unsigned char output[24 * 640]__attribute__ ((aligned(128)));
	mfc_get(&buff, argp, sizeof(buff), TAG, 0, 0);
	mfc_write_tag_mask(1<<TAG);
	mfc_read_tag_status_all();
	sobel(buff, output);
	mfc_put(output, argp, sizeof(output), TAG, 0, 0);
	mfc_write_tag_mask(1<<TAG);
	mfc_read_tag_status_all();
	return 0;
}
